﻿namespace OnlineBrowser.Logic.Html
{
    public interface ILinkTransformer
    {
        string FullyQualityRelative(string url, string html);
        string ConvertAllLinksToBrowseQueryUrl(string html);
    }
}