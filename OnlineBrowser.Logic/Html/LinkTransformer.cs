﻿using System;
using System.Web;
using HtmlAgilityPack;
using OnlineBrowser.Logic.Url;

namespace OnlineBrowser.Logic.Html
{
    public class LinkTransformer : ILinkTransformer
    {
        public string FullyQualityRelative(string rootUrl, string html)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);
            var nodes = doc.DocumentNode?.SelectNodes("//a[@href]");
            if (nodes == null)
                return html;

            foreach (HtmlNode link in nodes)
            {
                HtmlAttribute att = link.Attributes["href"];
                att.Value = att.Value.StartsWith("/") 
                    ? UrlRootHelper.AddRelativePath(rootUrl, att.Value)
                    : att.Value;
            }
            return doc.DocumentNode.OuterHtml;
        }

        public string ConvertAllLinksToBrowseQueryUrl(string html)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);
            var nodes = doc.DocumentNode?.SelectNodes("//a[@href]");
            if (nodes == null)
                return html;

            foreach (HtmlNode link in nodes)
            {
                HtmlAttribute att = link.Attributes["href"];
                att.Value = $"/Browser?url={HttpUtility.UrlEncode(att.Value)}";
            }
            return doc.DocumentNode.OuterHtml;
        }
    }
}