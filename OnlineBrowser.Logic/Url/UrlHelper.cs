﻿using System;

namespace OnlineBrowser.Logic.Url
{
    public static class UrlRootHelper
    {
        public static string GetUrlRoot(string url)
        {
            Uri uri = new Uri(url);
            return uri.Scheme + "://" + uri.Authority;
        }

        public static string AddRelativePath(string rootUrl, string relativePath)
        {
            Uri rootUri = new Uri(rootUrl);
            return new Uri(rootUri, relativePath).ToString();
        }
    }
}