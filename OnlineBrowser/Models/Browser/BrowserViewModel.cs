﻿using System.Web;

namespace OnlineBrowser.Web.Models.Browser
{
    public class BrowserViewModel
    {
        public string HtmlContent { get; set; }
    }
}