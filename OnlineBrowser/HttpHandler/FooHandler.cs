﻿using System;
using System.Web;
using Autofac;

namespace OnlineBrowser.Web.HttpHandler
{
    /// <summary>
    /// Gets the site-wide custom CSS for this rewardsCurrentPartner's rewards program
    /// </summary>
    public class FooHandler : IHttpAsyncHandler
    {
        public FooHandler()
        {

        }

        public bool IsReusable => true;

        protected delegate void AsyncProcessorDelegate(HttpContext context);
        AsyncProcessorDelegate _worker;

        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            _worker = ProcessRequest;
            return _worker.BeginInvoke(context, cb, extraData);
        }

        public void EndProcessRequest(IAsyncResult result)
        {
            _worker.EndInvoke(result);
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Write("test");
            context.Response.ContentType = "text/css";
        }
    }
}