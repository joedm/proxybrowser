﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OnlineBrowser.Logic;
using OnlineBrowser.Logic.Html;
using OnlineBrowser.Logic.Mime;
using OnlineBrowser.Logic.Url;
using OnlineBrowser.Web.Models.Browser;

namespace OnlineBrowser.Web.Controllers
{
    public class BrowserController : Controller
    {
        // GET: Browser
        public ActionResult Index(string url)
        {
            CrazyStatic.LastRoot = UrlRootHelper.GetUrlRoot(url);
            BrowserViewModel model = new BrowserViewModel();

            string html;
            string urlRoot = UrlRootHelper.GetUrlRoot(url);

            using (WebClient client = new WebClient())
            {
                client.Headers.Add("User-Agent", Request.Headers.GetValues("User-Agent")?.First());

                string mimeType = ContentTypeHelper.GetMimeType(url.Split('?')[0]);

                if (mimeType != "application/octet-stream")
                {
                    byte[] data = client.DownloadData(url);
                    return File(data, client.ResponseHeaders[HttpResponseHeader.ContentType]);
                }
                
                html = client.DownloadString(url);
            }

            ILinkTransformer linkTransformer = new LinkTransformer();
            html = linkTransformer.FullyQualityRelative(urlRoot, html);
            model.HtmlContent = linkTransformer.ConvertAllLinksToBrowseQueryUrl(html);
            
            return View(model);
        }

        public ActionResult Search(string searchString)
        {
            searchString = searchString.ToLower();
            string url = searchString.StartsWith("http://") || searchString.StartsWith("https://")
                ? searchString
                : $"https://www.google.com.au/search?q={searchString}";
            return RedirectToAction("Index", "Browser", new { url });
        }

        public ActionResult FixRelativeLinks(string url)
        {
            if (string.IsNullOrEmpty(url))
                return RedirectToAction("Index", "Home");

            string newUrl = BuildFullPathFromReferrer(Request.Url, Request.UrlReferrer);

            return RedirectToAction("Index", "Browser", new { url = newUrl });
        }

        private string GetUrlFromQueryString(string fullUrl)
        {
            Uri fullUri = new Uri(fullUrl);
            string encodedUrl = fullUri.Query.ToLower().Replace("?url=", "");
            return HttpUtility.UrlDecode(encodedUrl);
        }

        private string BuildFullPathFromReferrer(Uri requestUri, Uri referrerUri)
        {
            string referrerQueryUrl = referrerUri == null ? CrazyStatic.LastRoot : GetUrlFromQueryString(referrerUri.ToString());
            if (string.IsNullOrEmpty(referrerQueryUrl))
                referrerQueryUrl = CrazyStatic.LastRoot;

            Uri referrerQueryUri =  new Uri(referrerQueryUrl);
            Uri result = new Uri(referrerQueryUri, requestUri.PathAndQuery);

            return result.ToString();
        }
    }
}